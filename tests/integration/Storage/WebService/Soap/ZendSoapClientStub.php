<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 9:15 AM
 */

namespace Tests\Smorken\SisAuth\integration\Storage\WebService\Soap;

use Smorken\Soap\Zend\Client;
use Zend\Soap\Exception\ExceptionInterface;
use Zend\Soap\Exception\UnexpectedValueException;

class ZendSoapClientStub extends Client implements \Smorken\Soap\Contracts\Soap\Client
{

    /**
     * Initialize SOAP Client object
     *
     * @throws ExceptionInterface
     */
    protected function initSoapClientObject()
    {
        $wsdl = $this->getWSDL();
        $options = array_merge($this->getOptions(), ['trace' => true]);
        if ($wsdl === null) {
            if (!isset($options['location'])) {
                throw new UnexpectedValueException('"location" parameter is required in non-WSDL mode.');
            }
            if (!isset($options['uri'])) {
                throw new UnexpectedValueException('"uri" parameter is required in non-WSDL mode.');
            }
        } else {
            if (isset($options['use'])) {
                throw new UnexpectedValueException('"use" parameter only works in non-WSDL mode.');
            }
            if (isset($options['style'])) {
                throw new UnexpectedValueException('"style" parameter only works in non-WSDL mode.');
            }
        }
        unset($options['wsdl']);
        $this->soapClient = new SoapClientStub($wsdl, $options);
    }
}

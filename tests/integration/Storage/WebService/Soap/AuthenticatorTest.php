<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 9:08 AM
 */

namespace Tests\Smorken\SisAuth\integration\Storage\WebService\Soap;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Logging\Log;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Sis\WebService\IB\Soap\Models\Auth\User;
use Smorken\SisAuth\Storage\WebService\Soap\Authenticator;
use Smorken\Soap\Contracts\Soap\Model;

class AuthenticatorTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testRetrieveByCredentialsValid()
    {
        $this->getClientClass($this->getResponseXml());
        list($sut, $app, $config) = $this->getSut();
        $app->shouldReceive('environment')->andReturn('production');
        $this->setConfigOptions($config);
        $r = $sut->retrieveByCredentials('abc1234567', '123');
        $expected = [
            'id' => '30000000',
            'username' => 'ABC1234567',
            'first_name' => 'Joe',
            'last_name' => 'Bob',
            'email' => 'nomail1@example.com',
        ];
        $this->assertEquals($expected, $r->getAttributes());
    }

    public function testRetrieveByCredentialsNoMeidInResponse()
    {
        $this->getClientClass($this->getResponseXml('nomeid'));
        list($sut, $app, $config) = $this->getSut();
        $app->shouldReceive('environment')->andReturn('production');
        $this->setConfigOptions($config);
        $r = $sut->retrieveByCredentials('abc', '123');
        $expected = [
            'id' => '30000000',
            'username' => 'ABC',
            'first_name' => 'Joe',
            'last_name' => 'Bob',
            'email' => 'nomail1@example.com',
        ];
        $this->assertEquals($expected, $r->getAttributes());
    }

    public function testRetrieveByCredentialsNoMeidStripsCharacters()
    {
        $this->getClientClass($this->getResponseXml('nomeid'));
        list($sut, $app, $config) = $this->getSut();
        $app->shouldReceive('environment')->andReturn('production');
        $this->setConfigOptions($config);
        $r = $sut->retrieveByCredentials('abc<script>alert();</script>', '123');
        $expected = [
            'id' => '30000000',
            'username' => 'ABCSCRIPTALERTSCRIPT',
            'first_name' => 'Joe',
            'last_name' => 'Bob',
            'email' => 'nomail1@example.com',
        ];
        $this->assertEquals($expected, $r->getAttributes());
    }

    public function testRetrieveByCredentialsIsNullWhenAuthFails()
    {
        $this->getClientClass($this->getIncorrectXml());
        list($sut, $app, $config, $logger) = $this->getSut();
        $app->shouldReceive('environment')->andReturn('production');
        $this->setConfigOptions($config);
        $logger->shouldReceive('error')->once()->with('{"MSGS":{"MSG":{"ID":"14098-177","DESCR":"User ABC1234567 could not be authenticated","MESSAGE_SEVERITY":"E","PROPS":""}}}');
        $r = $sut->retrieveByCredentials('abc', '123');
        $this->assertNull($r);
    }

    public function testRetrieveByCredentialsIsExceptionWhenNotAuthFails()
    {
        $this->getClientClass($this->getErrorXml());
        list($sut, $app, $config, $logger) = $this->getSut();
        $app->shouldReceive('environment')->andReturn('production');
        $this->setConfigOptions($config);
        $logger->shouldReceive('error')->once()->with('{"MSGS":{"MSG":{"ID":"14098-123","DESCR":"Some Error","MESSAGE_SEVERITY":"E","PROPS":""}}}');
        $this->expectException(\SoapFault::class);
        $sut->retrieveByCredentials('abc', '123');
    }

    protected function getSut()
    {
        $sut = new Authenticator();
        list($model) = $this->getModel(User::class);
        $sut->setModel($model);
        $app = m::mock(Application::class);
        $config = m::mock(Repository::class);
        $logger = m::mock(Log::class);
        $sut->setApp($app);
        $sut->setConfig($config);
        $sut->setLogger($logger);
        return [$sut, $app, $config, $logger];
    }

    protected function setConfigOptions(Repository $config)
    {
        $config->shouldReceive('get')->with('authsis.soap.auth.middleware.wsse.username', null)->andReturn('foouser');
        $config->shouldReceive('get')->with('authsis.soap.auth.middleware.wsse.password', null)->andReturn('foopass');
        $config->shouldReceive('get')->with('authsis.soap.auth.middleware.soap_action', '%s')->andReturn(
            '%s.foo_action'
        );
        $config->shouldReceive('get')->with('authsis.soap.auth.wsdl', null)->andReturn(null);
        $config->shouldReceive('get')->with('authsis.soap.auth.function', 'SCC_USERREG_AUTHENTICATE')->andReturn(
            'FOO_FUNCTION'
        );
        $opts = [
            'location' => 'https://example.org',
            'uri'      => 'http://xmlns.oracle.com/Enterprise/HCM/services/URI.1',
            'trace'    => 1,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
        ];
        $config->shouldReceive('get')->with('authsis.soap.auth.client_options', [])->andReturn($opts);
    }

    protected function getClientClass($response = null)
    {
        if (!is_null($response)) {
            SoapClientStub::setResponse($response);
        }
        return ZendSoapClientStub::class;
    }

    protected function getModel($model_cls)
    {
        /**
         * @var Model $m
         */
        $m = new $model_cls;
        $m->setWsdl(null);//$this->getWsdl());
        $this->addConfigToModel($m);
        $model_cls::setClientClass($this->getClientClass());
        return [$m];
    }

    protected function addConfigToModel($m)
    {
        $c = m::mock(Repository::class);
        $c->shouldReceive('get')->with('soap.client_options', [])->andReturn([]);
        $c->shouldReceive('get')->with('soap.soap_options', [])->andReturn([]);
        $m->setConfig($c);
    }

    protected function getResponseXml($type = 'success')
    {
        return file_get_contents(sprintf('%s/../../../fixtures/Auth/response.%s.xml', __DIR__, $type));
    }

    protected function getErrorXml()
    {
        return file_get_contents(__DIR__ . '/../../../fixtures/Auth/response.error.xml');
    }

    protected function getIncorrectXml()
    {
        return file_get_contents(__DIR__ . '/../../../fixtures/Auth/response.incorrect.xml');
    }

    protected function getWsdl()
    {
        return null;
    }

    protected function getRequestXml()
    {
        return file_get_contents(__DIR__ . '/../../../fixtures/Auth/request.xml');
    }
}

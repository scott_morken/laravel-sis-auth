<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 10:29 AM
 */

namespace Tests\Smorken\SisAuth\unit\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Session\Session;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Exceptions\AuthDisplayableException;
use Smorken\Auth\User\Contracts\Models\User;
use Smorken\SisAuth\Contracts\Storage\Authenticator;
use Smorken\SisAuth\Contracts\Storage\Backend;
use Smorken\SisAuth\Providers\Sis;

class SisTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testRetrieveByTokenIsProviderValue()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $bp->shouldReceive('getByToken')->once()->with('123', 'Token')->andReturn('value');
        $this->assertEquals('value', $sut->retrieveByToken('123', 'Token'));
    }

    public function testUpdateRememberTokenDoesNothing()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $bp->shouldReceive('store')->never();
        $user = m::mock(User::class);
        $this->assertNull($sut->updateRememberToken($user, 'Token'));
    }

    public function testRetrieveByIdNoResultIsNull()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $bp->shouldReceive('getById')->once()->with('123')->andReturnNull();
        $this->assertNull($sut->retrieveById('123'));
    }

    public function testRetrieveByIdWithResultIsAuthenticatable()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $user = m::mock(User::class);
        $bp->shouldReceive('getById')->once()->with('123')->andReturn($user);
        $this->assertEquals($user, $sut->retrieveById('123'));
    }

    public function testRetrieveByIdWithResultCanCache()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut(true);
        $user = m::mock(User::class);
        $cp->shouldReceive('remember')->once()->with('sisauth.rid.123', 5, m::type('callable'))->andReturn($user);
        $this->assertEquals($user, $sut->retrieveById('123'));
    }

    public function testRetrieveByCredentialsFailsIsExceptionAndFlushesSession()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $creds = ['username' => 'abc', 'password' => '123'];
        $ap->shouldReceive('retrieveByCredentials')->once()->with('abc', '123')->andReturnNull();
        $sp->shouldReceive('flush')->once();
        $this->expectException(AuthDisplayableException::class);
        $this->expectExceptionMessage('The credentials provided for [abc] do not match any users.');
        $sut->retrieveByCredentials($creds);
    }

    public function testRetrieveByCredentialsSuccessStoresAndReturnsUser()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $user = m::mock(User::class);
        $creds = ['username' => 'abc', 'password' => '123'];
        $ap->shouldReceive('retrieveByCredentials')->once()->with('abc', '123')->andReturn($user);
        $sp->shouldReceive('flush')->once();
        $bp->shouldReceive('store')->once()->with($user)->andReturn(true);
        $this->assertEquals($user, $sut->retrieveByCredentials($creds));
    }

    public function testCheckCredentialsWithoutUsernameIsExceptionAndFlushesSession()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $creds = [];
        $ap->shouldReceive('retrieveByCredentials')->never();
        $sp->shouldReceive('flush')->once();
        $bp->shouldReceive('store')->never();
        $this->expectException(AuthDisplayableException::class);
        $this->expectExceptionMessage('Login attribute [username] not provided.');
        $sut->retrieveByCredentials($creds);
    }

    public function testCheckCredentialsWithoutPasswordIsExceptionAndFlushesSession()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $creds = ['username' => 'abc'];
        $ap->shouldReceive('retrieveByCredentials')->never();
        $sp->shouldReceive('flush')->once();
        $bp->shouldReceive('store')->never();
        $this->expectException(AuthDisplayableException::class);
        $this->expectExceptionMessage('Login attribute [password] not provided.');
        $sut->retrieveByCredentials($creds);
    }

    public function testValidateCredentialsMatchingUserIsTrue()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $user = m::mock(User::class);
        $user->shouldReceive('getAuthIdentifier')->twice()->andReturn('123');
        $authuser = m::mock(Authenticatable::class);
        $authuser->shouldReceive('getAuthIdentifier')->once()->andReturn('123');
        $creds = ['username' => 'abc', 'password' => '123'];
        $ap->shouldReceive('retrieveByCredentials')->once()->with('abc', '123')->andReturn($user);
        $sp->shouldReceive('flush')->once();
        $bp->shouldReceive('store')->once()->with($user)->andReturn(true);
        $this->assertTrue($sut->validateCredentials($authuser, $creds));
    }

    public function testValidateCredentialsNotMatchingUserIsFalseAndFlushesSession()
    {
        list($sut, $ap, $bp, $sp, $cp) = $this->getSut();
        $user = m::mock(User::class);
        $user->shouldReceive('getAuthIdentifier')->twice()->andReturn('123');
        $authuser = m::mock(Authenticatable::class);
        $authuser->shouldReceive('getAuthIdentifier')->once()->andReturn('345');
        $creds = ['username' => 'abc', 'password' => '123'];
        $ap->shouldReceive('retrieveByCredentials')->once()->with('abc', '123')->andReturn($user);
        $sp->shouldReceive('flush')->twice();
        $bp->shouldReceive('store')->once()->with($user)->andReturn(true);
        $this->assertFalse($sut->validateCredentials($authuser, $creds));
    }

    protected function getSut($cache = false)
    {
        $ap = m::mock(Authenticator::class);
        $bp = m::mock(Backend::class);
        $sp = m::mock(Session::class);
        $cp = $cache ? m::mock(Repository::class) : null;
        $sut = new Sis($ap, $bp, $sp, $cp);
        return [$sut, $ap, $bp, $sp, $cp];
    }
}

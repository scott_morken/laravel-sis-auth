<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 10:19 AM
 */

namespace Tests\Smorken\SisAuth\unit\Storage\Eloquent\MyCC;

use Illuminate\Contracts\Auth\Authenticatable;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\User\Contracts\Models\User;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Base\Person\Person;
use Smorken\SisAuth\Storage\Eloquent\MyCC\Backend;

class BackendTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testGetByTokenIsNull()
    {
        list($sut, $m) = $this->getSut();
        $this->assertNull($sut->getByToken('123', 'Token'));
    }

    public function testGetByIdIsNullWithNoResult()
    {
        list($sut, $m) = $this->getSut();
        $m->shouldReceive('newQuery->emplidIs->first')->once()->andReturn(null);
        $this->assertNull($sut->getById('123'));
    }

    public function testGetByIdConvertsPersonToAuthenticatable()
    {
        list($sut, $m) = $this->getSut();
        $person = m::mock(Person::class);
        $person->id = '123';
        $person->alt_id = 'ABC1234567';
        $person->first_name = 'Joe';
        $person->last_name = 'Bob';
        $person->email = 'email@example.org';
        $m->shouldReceive('newQuery->emplidIs->first')->once()->andReturn($person);
        $user = $sut->getById('123');
        $this->assertInstanceOf(Authenticatable::class, $user);
        $this->assertEquals('123', $user->getAuthIdentifier());
    }

    public function testStoreDoesNothingAndReturnsTrue()
    {
        list($sut, $m) = $this->getSut();
        $user = m::mock(User::class);
        $this->assertTrue($sut->store($user));
    }

    protected function getSut()
    {
        $sp = m::mock(Model::class);
        $sut = new Backend();
        $sut->setModel($sp);
        return [$sut, $sp];
    }
}

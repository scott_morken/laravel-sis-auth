<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 10:09 AM
 */

namespace Tests\Smorken\SisAuth\unit\Storage\Session;

use Illuminate\Contracts\Session\Session;
use PHPUnit\Framework\TestCase;
use Mockery as m;
use Smorken\Auth\User\Contracts\Models\User;
use Smorken\SisAuth\Storage\Session\Backend;

class BackendTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testGetByTokenIsNull()
    {
        list($sut, $s) = $this->getSut();
        $this->assertNull($sut->getByToken('123', 'Token'));
    }

    public function testGetByIdWithNothingStoredIsNull()
    {
        list($sut, $s) = $this->getSut();
        $s->shouldReceive('has')->once()->with('sisauth_202cb962ac59075b964b07152d234b70')->andReturn(false);
        $this->assertNull($sut->getById('123'));
    }

    public function testGetByIdWithNothingStoredIsResult()
    {
        list($sut, $s) = $this->getSut();
        $s->shouldReceive('has')->once()->with('sisauth_202cb962ac59075b964b07152d234b70')->andReturn(true);
        $s->shouldReceive('get')->once()->with('sisauth_202cb962ac59075b964b07152d234b70')->andReturn('user_model');
        $this->assertEquals('user_model', $sut->getById('123'));
    }

    public function testStorePutsIntoSession()
    {
        list($sut, $s) = $this->getSut();
        $user = m::mock(User::class);
        $user->shouldReceive('getAuthIdentifier')->once()->andReturn('123');
        $s->shouldReceive('put')->once()->with('sisauth_202cb962ac59075b964b07152d234b70', $user);
        $this->assertTrue($sut->store($user));
    }

    protected function getSut()
    {
        $sp = m::mock(Session::class);
        $sut = new Backend();
        $sut->setModel($sp);
        return [$sut, $sp];
    }
}

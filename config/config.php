<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 3:09 PM
 */
return [
    'providers' => [
        'authenticator' => [
            'impl'  => [
                'type'   => 'instantiate', //'di',
                'actual' => \Smorken\SisAuth\Storage\WebService\Soap\Authenticator::class,
            ],
            'model' => [
                'type'   => 'instantiate',
                'actual' => \Smorken\Sis\WebService\IB\Soap\Models\Auth\User::class,
            ],
        ],
        'backend'       => [
            'impl'  => [
                'type'   => 'instantiate',
                'actual' => \Smorken\SisAuth\Storage\Session\Backend::class,
            ],
            'model' => [
                'type'   => 'di',
                'actual' => \Illuminate\Contracts\Session\Session::class,
            ],
        ],
    ],
    'soap'      => [
        'auth' => [
            'wsdl'           => null,//'https://url.com/sap.wsdl',
            'function'       => 'SCC_UR_AUTHENTICATE_REQ',
            'middleware'     => [
                'soap_action' => '%s.VERSION_1#foobar',
                'wsse'        => [
                    'username' => 'username',
                    'password' => 'password',
                ],
            ],
            'client_options' => [
                'location' => 'https://foo.bar',
                'uri'      => 'http://xmlns.targetnamespace',
            ],
        ],
    ],
];

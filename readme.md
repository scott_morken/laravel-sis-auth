Add service provider line to config/app.php

```
'providers' => array(
    ...
    'Smorken\SisAuth\ServiceProvider',
```

Copy vendor config files

```
$ php artisan vendor:publish --provider="\Smorken\SisAuth\ServiceProvider"
```

Provider tag is `sis`


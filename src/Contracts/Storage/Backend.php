<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 6:50 AM
 */

namespace Smorken\SisAuth\Contracts\Storage;

use Smorken\Auth\User\Contracts\Models\User;

interface Backend extends Base
{

    /**
     * @param $id
     * @return User|null
     */
    public function getById($id);

    /**
     * @param $id
     * @param $token
     * @return User|null
     */
    public function getByToken($id, $token);

    /**
     * @param User $model
     * @return bool
     */
    public function store(User $model);
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 6:51 AM
 */

namespace Smorken\SisAuth\Contracts\Storage;

use Smorken\Auth\User\Contracts\Models\User;

interface Authenticator extends Base
{

    /**
     * @param $username
     * @param $password
     * @return null|User
     */
    public function retrieveByCredentials($username, $password);
}

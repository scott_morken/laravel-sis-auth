<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 9:02 AM
 */

namespace Smorken\SisAuth\Contracts\Storage;

interface Base
{

    /**
     * @param $model
     * @return void
     */
    public function setModel($model);
}

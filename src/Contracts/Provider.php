<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/8/17
 * Time: 12:00 PM
 */

namespace Smorken\SisAuth\Contracts;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Session\Session;
use Smorken\SisAuth\Contracts\Storage\Authenticator;
use Smorken\SisAuth\Contracts\Storage\Backend;

interface Provider extends UserProvider
{

    /**
     * @return Backend
     */
    public function getBackendProvider();

    /**
     * @return Authenticator
     */
    public function getAuthenticationProvider();

    /**
     * @return Session
     */
    public function getSessionProvider();

    public function setUsernameField($field);

    public function setPasswordField($field);
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/8/17
 * Time: 12:03 PM
 */

namespace Smorken\SisAuth\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Session\Session;
use Smorken\Auth\Exceptions\AuthDisplayableException;
use Smorken\Auth\User\Contracts\Models\User;
use Smorken\SisAuth\Contracts\Provider;
use Smorken\SisAuth\Contracts\Storage\Authenticator;
use Smorken\SisAuth\Contracts\Storage\Backend;

class Sis implements Provider
{

    /**
     * @var Backend
     */
    protected $backend;

    /**
     * @var Authenticator
     */
    protected $authenticator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Repository
     */
    protected $cache;

    /**
     * @var User
     */
    protected $retByCreds;

    protected $fields = [
        'username' => 'username',
        'password' => 'password',
    ];

    public function __construct(Authenticator $authenticator, Backend $backend, Session $session, Repository $cache = null)
    {
        $this->authenticator = $authenticator;
        $this->backend = $backend;
        $this->session = $session;
        $this->cache = $cache;
    }

    public function setPasswordField($field)
    {
        $this->setField('username', $field);
    }

    public function setUsernameField($field)
    {
        $this->setField('password', $field);
    }

    protected function setField($name, $value)
    {
        $this->fields[$name] = $value;
    }

    protected function getField($name)
    {
        return array_get($this->fields, $name, null);
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        $key = 'sisauth.rid.' . $identifier;
        return $this->remember(
            $key,
            5,
            function () use ($identifier) {
                return $this->getBackendProvider()->getById($identifier);
            }
        );
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        return $this->getBackendProvider()->getByToken($identifier, $token);
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        return;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return Authenticatable|null
     * @throws AuthDisplayableException
     */
    public function retrieveByCredentials(array $credentials)
    {
        list($username, $password) = $this->checkCredentials($credentials);
        $this->getSessionProvider()->flush();
        $user = $this->getAuthenticationProvider()->retrieveByCredentials(
            $username,
            $password
        );
        if (!$user) {
            throw new AuthDisplayableException(
                "The credentials provided for [$username] do not match any users.",
                "Invalid username and/or password."
            );
        } else {
            $this->retByCreds = $user;
            $this->getBackendProvider()->store($user);
        }
        return $user;
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $retrieved = $this->handleCredentials($credentials);
        $valid = ($retrieved instanceof Authenticatable &&
            $retrieved->getAuthIdentifier() &&
            $retrieved->getAuthIdentifier() == $user->getAuthIdentifier()
        );
        if (!$valid) {
            $this->getSessionProvider()->flush();
        }
        return $valid;
    }

    /**
     * @param $credentials
     * @return Authenticatable|null|User
     */
    protected function handleCredentials($credentials)
    {
        $this->checkCredentials($credentials);
        if ($this->retByCreds) {
            $username_field = $this->getField('username');
            $username = $credentials[$username_field];
            if (strtoupper($username) === strtoupper($this->retByCreds->username)) {
                return $this->retByCreds;
            }
            $this->retByCreds = null;
        } else {
            return $this->retrieveByCredentials($credentials);
        }
    }

    /**
     * @return Session
     */
    public function getSessionProvider()
    {
        return $this->session;
    }

    /**
     * @return Backend
     */
    public function getBackendProvider()
    {
        return $this->backend;
    }

    /**
     * @return Authenticator
     */
    public function getAuthenticationProvider()
    {
        return $this->authenticator;
    }

    protected function checkCredentials(array $credentials)
    {
        $username_field = $this->getField('username');
        if (!$username_field || !array_key_exists($username_field, $credentials)) {
            $this->getSessionProvider()->flush();
            throw new AuthDisplayableException(
                "Login attribute [$username_field] not provided.",
                'Username is required.'
            );
        }
        $password_field = $this->getField('password');
        if (!$password_field || !array_key_exists($password_field, $credentials)) {
            $this->getSessionProvider()->flush();
            throw new AuthDisplayableException(
                "Login attribute [$password_field] not provided.",
                'Password is required.'
            );
        }
        return [
            $credentials[$username_field],
            $credentials[$password_field],
        ];
    }

    protected function remember($key, $mins, \Closure $function)
    {
        if ($this->cache) {
            return $this->cache->remember(
                $key,
                $mins,
                $function
            );
        } else {
            return $function();
        }
    }
}

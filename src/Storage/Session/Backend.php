<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 7:38 AM
 */

namespace Smorken\SisAuth\Storage\Session;

use Illuminate\Contracts\Session\Session;
use Smorken\Auth\User\Contracts\Models\User;
use Smorken\SisAuth\Storage\Base;

class Backend extends Base implements \Smorken\SisAuth\Contracts\Storage\Backend
{

    /**
     * @var Session
     */
    protected $model;

    protected $base = 'sisauth';

    /**
     * @param $id
     * @return User|null
     */
    public function getById($id)
    {
        $key = $this->getKey($id);
        if ($this->model->has($key)) {
            return $this->convert($this->model->get($key));
        }
        return null;
    }

    /**
     * @param $id
     * @param $token
     * @return User|null
     */
    public function getByToken($id, $token)
    {
        return null;
    }

    /**
     * @param User $model
     * @return bool
     */
    public function store(User $model)
    {
        $key = $this->getKey($model->getAuthIdentifier());
        $this->model->put($key, $model);
        return true;
    }

    /**
     * @param $result
     * @return User
     */
    protected function convert($result)
    {
        return $result;
    }

    protected function getKey($id)
    {
        return $this->base . '_' . md5($id);
    }
}

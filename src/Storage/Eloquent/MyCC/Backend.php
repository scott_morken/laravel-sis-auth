<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 7:28 AM
 */

namespace Smorken\SisAuth\Storage\Eloquent\MyCC;

use Smorken\Auth\User\Contracts\Models\User;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Base\Person\Person;
use Smorken\SisAuth\Storage\Base;

class Backend extends Base implements \Smorken\SisAuth\Contracts\Storage\Backend
{

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param $id
     * @return User|null
     */
    public function getById($id)
    {
        return $this->convert(
            $this->model
                ->newQuery()
                ->emplidIs($id)
                ->first()
        );
    }

    /**
     * @param $id
     * @param $token
     * @return User|null
     */
    public function getByToken($id, $token)
    {
        return null;
    }

    /**
     * @param User $model
     * @return bool
     */
    public function store(User $model)
    {
        // no write allowed
        return true;
    }

    /**
     * @param Model $result
     * @return User
     */
    protected function convert($result)
    {
        if ($result instanceof Person) {
            $user = new \Smorken\Auth\User\Models\VO\User();
            $id = $user->getAuthIdentifierName();
            $user->$id = $result->id;
            $user->username = $result->alt_id;
            $user->first_name = $result->first_name;
            $user->last_name = $result->last_name;
            $user->email = $result->email;
            return $user;
        }
        return null;
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 8:44 AM
 */

namespace Smorken\SisAuth\Storage\Eloquent;

use Smorken\SisAuth\Contracts\Storage\Backend;

class User extends \Smorken\Auth\User\Storage\Eloquent\User implements Backend
{

    /**
     * @var User
     */
    protected $model;

    /**
     * @param $id
     * @return \Smorken\Auth\User\Contracts\Models\User|null
     */
    public function getById($id)
    {
        return $this->convert($this->find($id));
    }

    /**
     * @param $id
     * @param $token
     * @return \Smorken\Auth\User\Contracts\Models\User|null
     */
    public function getByToken($id, $token)
    {
        return null;
    }

    /**
     * @param \Smorken\Auth\User\Contracts\Models\User $model
     * @return bool
     */
    public function store(\Smorken\Auth\User\Contracts\Models\User $model)
    {
        $attrs = [
            'id'         => $model->getAuthIdentifier(),
            'first_name' => $model->first_name,
            'last_name'  => $model->last_name,
            'email'      => $model->email,
            'username'   => $model->username,
        ];
        $model->fill($attrs);
        return $model->save();
    }

    /**
     * @param $result
     * @return \Smorken\Auth\User\Contracts\Models\User
     */
    protected function convert($result)
    {
        return $result;
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 7:30 AM
 */

namespace Smorken\SisAuth\Storage;

use Smorken\Auth\User\Contracts\Models\User;

abstract class Base
{

    protected $model;

    public function setModel($model)
    {
        $this->model = $model;
    }
    /**
     * @param $result
     * @return User
     */
    abstract protected function convert($result);
}

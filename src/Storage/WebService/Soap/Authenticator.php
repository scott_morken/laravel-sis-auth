<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 7:38 AM
 */

namespace Smorken\SisAuth\Storage\WebService\Soap;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Logging\Log;
use Smorken\Auth\User\Contracts\Models\User;
use Smorken\Sis\WebService\IB\Soap\Types\Auth\SCC_UR_AUTHENTICATE_REQ;
use Smorken\SisAuth\Storage\Base;
use Smorken\Soap\Contracts\Soap\Model;
use Smorken\Soap\RequestObject;
use Smorken\Soap\Wsse\WsseSoapHeader;

class Authenticator extends Base implements \Smorken\SisAuth\Contracts\Storage\Authenticator
{

    /**
     * @var Repository
     */
    protected $config;

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Log
     */
    protected $logger;

    /**
     * @var Model
     */
    protected $model;

    protected $base = 'authsis.soap.auth';

    /**
     * @param $username
     * @param $password
     * @return null|User
     * @throws \SoapFault
     */
    public function retrieveByCredentials($username, $password)
    {
        $wuser = $this->getConfig()
                      ->get($this->getConfigKey('middleware.wsse.username'), null);
        $wpass = $this->getConfig()
                      ->get($this->getConfigKey('middleware.wsse.password'), null);
        $wsdl = $this->getWsdl();
        $func = $this->getFunction();
        $action_mod = $this->getSoapActionModifier();
        $wsse = $this->getWsse($wuser, $wpass);
        $req = $this->getRequest($username, $password);
        $model = $this->getModel();
        $model->setSoapOptions(
            $this->getClientOptions($wsdl)
        );
        $model->setWsdl($wsdl);
        try {
            $q = $model->newRequest()
                       ->asFunction($func, $req)
                       ->modifier('action', $action_mod)
                       ->modifier('request', [$this->replace('ns1:'), $this->stripTag($func)])
                       ->soapHeader($wsse);
            if ($this->logger) { //not null during testing
                $q->setLogger($this->getLogger());
            }
            $result = $q->first();
        } catch (\SoapFault $e) {
            if (property_exists($e, 'detail') && $this->checkFailedAuth($e->detail)) {
                return null;
            } else {
                throw $e;
            }
        }
        return $this->verifyUsername($this->convert($result), $username);
    }

    protected function verifyUsername($model, $username)
    {
        if (!is_null($model) && !$model->alt_id) {
            $username = strtoupper(preg_replace('/[^A-z0-9]/', '', $username));
            $model->username = $username;
        }
        return $model;
    }

    protected function checkFailedAuth($detail)
    {
        if ($detail instanceof \stdClass) {
            $props = get_object_vars($detail);
            foreach ($props as $name => $value) {
                if ($name === 'DESCR') {
                    return stripos($detail->DESCR, 'could not be authenticated') !== false;
                }
                if ($this->checkFailedAuth($value) === true) {
                    return true;
                }
            }
        } elseif (is_array($detail)) {
            foreach ($detail as $k => $v) {
                return $this->checkFailedAuth($v);
            }
        }
        return false;
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    protected function getRequest($username, $password)
    {
        $auth = new SCC_UR_AUTHENTICATE_REQ(strtoupper($username), $password);
        $req = new RequestObject($auth, 'SCC_UR_AUTHENTICATE_REQ');
        return $req->getRequest(true);
    }

    protected function getFunction()
    {
        return $this->getConfig()
                    ->get($this->getConfigKey('function'), 'SCC_USERREG_AUTHENTICATE');
    }

    /**
     * @param \Smorken\Sis\Contracts\Ws\Base\Auth\User $result
     * @return User
     */
    protected function convert($result)
    {
        if ($result instanceof \Smorken\Sis\Contracts\Ws\Base\Auth\User) {
            $user = new \Smorken\Auth\User\Models\VO\User();
            $id = $user->getAuthIdentifierName();
            $user->$id = $result->id;
            $user->username = $result->alt_id;
            $user->first_name = $result->first_name;
            $user->last_name = $result->last_name;
            $user->email = $result->email;
            return $user;
        }
        return null;
    }

    protected function replace($str)
    {
        return function ($value) use ($str) {
            return str_ireplace($str, '', $value);
        };
    }

    protected function stripTag($tag)
    {
        return function ($value) use ($tag) {
            $doc = new \DOMDocument();
            $doc->loadXML($value);
            $list = $doc->getElementsByTagName($tag);
            foreach ($list as $ele) {
                $children = $ele->childNodes;
                $parent = $ele->parentNode;
                $parent->removeChild($ele);
                if ($children && $children instanceof \DOMNodeList) {
                    foreach ($children as $child) {
                        $parent->appendChild($child);
                    }
                }
            }
            return $doc->saveXML();
        };
    }

    protected function getWsse($user, $pass)
    {
        return new WsseSoapHeader($user, $pass);
    }

    protected function getSoapActionModifier()
    {
        $sa_string = $this->getConfig()
                          ->get($this->getConfigKey('middleware.soap_action'), '%s');
        return function ($value) use ($sa_string) {
            return sprintf($sa_string, $value);
        };
    }

    protected function getConfig()
    {
        if (!$this->config) {
            $this->config = $this->getApp()
                                 ->make(Repository::class);
        }
        return $this->config;
    }

    public function setConfig(Repository $config)
    {
        $this->config = $config;
    }

    protected function getLogger()
    {
        if (!$this->logger) {
            $this->logger = $this->getApp()
                                 ->make(Log::class);
        }
        return $this->logger;
    }

    public function setLogger(Log $logger)
    {
        $this->logger = $logger;
    }

    protected function getApp()
    {
        if (!$this->app) {
            $this->app = app();
        }
        return $this->app;
    }

    public function setApp(Application $app)
    {
        $this->app = $app;
    }

    protected function getConfigKey($key)
    {
        if (ends_with($this->base, '.')) {
            $this->base = substr($this->base, -1);
        }
        if (starts_with($key, '.')) {
            $key = substr($key, 1);
        }
        if ($this->base) {
            return sprintf('%s.%s', $this->base, $key);
        }
        return $key;
    }

    protected function getClientOptions($wsdl)
    {
        if (is_null($wsdl)) {
            return $this->getConfig()
                        ->get($this->getConfigKey('client_options'), []);
        }
        return $this->getConfig()
                    ->get('model.soap.soap_options', ['cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1]);
    }

    protected function getWsdl()
    {
        if ($this->getApp()
                 ->environment() === 'production') {
            return null;
        }
        return $this->getConfig()
                    ->get($this->getConfigKey('wsdl'), null);
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/9/17
 * Time: 8:54 AM
 */

namespace Smorken\SisAuth;

use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;
use Smorken\SisAuth\Contracts\Provider;
use Smorken\SisAuth\Contracts\Storage\Authenticator;
use Smorken\SisAuth\Contracts\Storage\Backend;
use Smorken\SisAuth\Providers\Sis;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        $this->bootConfig();
        Auth::provider(
            'sis',
            function ($app, array $config) {
                return $app[Provider::class];
            }
        );
    }

    protected function bootConfig()
    {
        $config = __DIR__ . '/../config/config.php';
        $this->mergeConfigFrom($config, 'authsis');
        $this->publishes([$config => config_path('authsis.php')], 'config');
    }

    public function register()
    {
        $this->bindProvider(Authenticator::class, 'authenticator');
        $this->bindProvider(Backend::class, 'backend');
        $this->app->bind(
            Provider::class,
            function ($app) {
                $ap = $app[Authenticator::class];
                $bp = $app[Backend::class];
                $sp = $app[Session::class];
                $cp = $app[Repository::class];
                return new Sis($ap, $bp, $sp, $cp);
            }
        );
    }

    protected function bindProvider($cls, $type)
    {
        $this->app->bind(
            $cls,
            function ($app) use ($type) {
                $c = $app['config'];
                $storeconfig = $c->get(sprintf('authsis.providers.%s.impl', $type), null);
                $modelconfig = $c->get(sprintf('authsis.providers.%s.model', $type), null);
                $impl = $this->buildFromConfig($storeconfig);
                $model = $this->buildFromConfig($modelconfig);
                $impl->setModel($model);
                return $impl;
            }
        );
    }

    protected function buildFromConfig($config)
    {
        $act = array_get($config, 'actual', null);
        $type = array_get($config, 'type', 'di');
        if ($act) {
            if ($type === 'di') {
                return $this->app[$act];
            }
            return new $act;
        }
    }
}
